# -*- coding: utf-8 -*
import os

# [Content] XML Footer Text
import time


def test_has_home_page_node():
    find_text_in_layout('首頁')


# [Behavior] Tap the coordinate on the screen
def test_tap_sidebar():
    tap_to_open_side_bar()


# 1. [Content] Side Bar Text
def test_has_side_bar_texts():
    find_text_in_layout('查看商品分類', '查訂單/退訂退款', '追蹤/買過/看過清單',
                        '智慧標籤', '其他', 'PChome 旅遊', '線上手機回收', '給24h購物APP評分')


# 2. [Screenshot] Side Bar Text
def test_side_bar_text_screenshot():
    screen_shot('Side-Bar-Text.png')


# 3. [Content] Categories
def test_categories():
    tap_to_close_side_bar()
    swipe_up()
    tap_to_expand_categories_panel()
    find_text_in_layout('精選', '3C', '周邊',
                        'NB', '通訊', '數位', '家電', '日用',
                        '食品', '生活', '運動戶外', '美妝', '衣鞋包錶')


# 4. [Screenshot] Categories
def test_categories_screenshot():
    screen_shot('Categories.png')


# 5. [Content] Categories page
def test_has_categories_page():
    tap_to_close_categories_panel()
    tap_bottom_tab_to_open_categories_page()


# 6. [Screenshot] Categories page
def test_categories_screenshot():
    time.sleep(1)  # turning to another page requires time
    screen_shot('Categories-Page.png')


# 7. [Behavior] Search item “switch”
def test_search_item_switch():
    input_in_search_bar('switch')


# 8. [Behavior] Follow an item and it should be add to the list
def test_follow_an_item_and_it_should_be_added_to_the_list():
    tap_the_first_search_item()
    favorite_the_current_viewing_item()
    go_back()
    go_back()
    go_back()  # go back to the home page that can view the side bar toggle
    tap_to_open_side_bar()
    tap_side_bar_item_favorite_viewed_list()
    find_text_in_layout('Nintendo Switch')
    pass


# 9. [Behavior] Navigate tto the detail of item
def test_navigate_to_details_of_item():
    tap_first_item_in_the_grid()
    swipe_up()
    tap_details_tab_of_product()


# 10. [Screenshot] Disconnetion Screen
def test_disconnection_screen_screenshot():
    disable_mobile_data()
    screen_shot('Disconnection.png')


def swipe_up():
    os.system('adb shell input touchscreen swipe 551 1543 551 381 200')
    time.sleep(2)


def tap_to_open_side_bar():
    tap(100, 100)
    time.sleep(2)


def tap_to_close_categories_panel():
    tap(933, 1065)


def tap_to_expand_categories_panel():
    tap(1005, 283)


def tap_to_close_side_bar():
    tap(945, 96)  # tap the right most side place to close the side bar


def tap_bottom_tab_to_open_categories_page():
    tap(331, 1705)


def input_in_search_bar(text):
    tap(535, 141)  # tap the search bar
    os.system('adb shell input text "{}"'.format(text))
    os.system('adb shell input keyevent "KEYCODE_ENTER"')
    time.sleep(5)  # searching requires time


def tap_the_first_search_item():
    tap(613, 573)
    time.sleep(3)  # loading requires time


def favorite_the_current_viewing_item():
    tap(109, 1701)
    time.sleep(2)


def tap_side_bar_item_favorite_viewed_list():
    tap(353, 865)


def tap_first_item_in_the_grid():
    tap(283, 791)
    time.sleep(2)


def tap_details_tab_of_product():
    tap(549, 133)


def tap(x, y):
    os.system('adb shell input tap {} {}'.format(x, y))
    time.sleep(0.8)


def go_back():
    os.system('adb shell input keyevent "KEYCODE_BACK"')
    time.sleep(1)


def disable_mobile_data():
    os.system('adb shell svc data disable')
    time.sleep(0.5)


def screen_shot(filename):
    os.system('adb shell screencap -p /sdcard/screen.png')
    os.system('adb pull /sdcard/screen.png')
    if os.path.exists(filename):
        os.remove(filename)
    os.rename('screen.png', filename)


def find_text_in_layout(*text):
    xmlString = dump_layout_xml()
    for text in text:
        assert xmlString.find(text) != -1, "Cannot find text " + text


def dump_layout_xml():
    os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml .')
    f = open('window_dump.xml', 'r', encoding="utf-8")
    return f.read()
